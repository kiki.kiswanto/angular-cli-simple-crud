import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { ContactHomeComponent } from '../contact/contact-home/contact-home.component';

const appRoutes: Routes = [
    {
        path: '',
        redirectTo: '/contact',
        pathMatch: 'full'
    },
    {
        path: 'contact',
        component: ContactHomeComponent,
        // canActivate: [FesRouterService]
    },
    // {
    //     path: '**',
    //     component: PageNotFoundComponent
    // }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes, { useHash: true }),
    ],
    providers: [
        
    ],
    declarations: [
        // start_declarations
        // end_declarations
    ],
    exports: [RouterModule]
})
export class AppRouteModule {
}