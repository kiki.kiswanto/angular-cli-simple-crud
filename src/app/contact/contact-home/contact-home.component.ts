import { Component, OnInit, ViewChild } from '@angular/core';
import { Contact, FormDataContact } from '../contact.model';
import { ContactFormComponent } from '../contact-form/contact-form.component';

@Component({
  selector: 'app-contact-home',
  templateUrl: './contact-home.component.html',
  styleUrls: ['./contact-home.component.css']
})
export class ContactHomeComponent implements OnInit {
  contactList : Contact[] = [];
  @ViewChild('comFormContact') comFormContact = new ContactFormComponent;

  constructor() { }

  ngOnInit() {
  }

  process(data : FormDataContact){
    if (data.index >= 0) {
      this.edit(data)
    } else {
      this.add(data);
    }
  }

  add(data : FormDataContact){
    this.contactList.push(data.data);
  }

  edit(data : FormDataContact){
    this.contactList[data.index].phoneNumber = data.data.phoneNumber;
    this.contactList[data.index].name = data.data.name;
    this.contactList[data.index].email = data.data.email;
  }

  processEdit(data : any){
    this.comFormContact.setEdit(data);
  }

}
