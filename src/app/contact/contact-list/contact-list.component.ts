import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Contact } from '../contact.model';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css']
})
export class ContactListComponent implements OnInit {
  @Input()
  contactList : Contact[] = [];
  @Output()
  onEdit = new EventEmitter();

  constructor() { }

  ngOnInit() {
    this.defaultData();
  }

  defaultData(){
    let contact : Contact = new Contact();
    contact.name = "John Doe";
    contact.phoneNumber = "08694858595";
    contact.email = "optimusprime@btpn.com";

    this.contactList.push(contact);
  }

  edit(index : number, data : any){
    this.onEdit.emit({ 'index' : index, 'data' : data });
  }

  delete(index : number){
    this.contactList.splice(index, 1);
  }

}
