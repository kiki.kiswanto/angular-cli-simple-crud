import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Contact, FormDataContact } from '../contact.model';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.css']
})
export class ContactFormComponent implements OnInit {
  index : number;
  formContact : FormGroup;
  @Output() outDataForm = new EventEmitter<FormDataContact>();

  constructor() { }

  ngOnInit() {
    this.initForm();
  }

  initForm(){
    this.formContact = new FormGroup({
      phoneNumber : new FormControl('', [Validators.required]),
      name : new FormControl('', [Validators.required]),
      email : new FormControl()
    })
  }

  setEdit(data : any){
    this.index = data.index;
    let formData : Contact = data.data;
    this.formContact.controls['phoneNumber'].setValue(formData.phoneNumber);
    this.formContact.controls['name'].setValue(formData.name);
    this.formContact.controls['email'].setValue(formData.email);
  }

  onSave(){
    let values = this.formContact.value;
    this.outDataForm.emit({
      'index' : this.index,
      'data' : values
    });
  }

  onReset(){
    this.initForm();
    this.index = null;
  }

}
